/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Jesus Alberto Venegas Lopez
 */
public class Parecederos extends Producto{
    private String fecha;
    private String temperatura;

    public Parecederos() {
        this.fecha = "";
        this.temperatura = "";
    }

    public Parecederos(String fecha, String temperatura) {
        this.fecha = fecha;
        this.temperatura = temperatura;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(String temperatura) {
        this.temperatura = temperatura;
    }
    
   
    @Override
    public float calcularPago() {
        float incremento = 0;
        switch (getUnidadProducto()) {
            case 1: 
                incremento = 0.03f;
                break;
            case 2: 
                incremento = 0.05f;
                break;
            case 3: 
                incremento = 0.04f;
                break;
            
        }
        
      
        float Incremento1 = getPrecioUnitario() * (1 + incremento);
        
        float precioFinal = Incremento1 * 1.50f;
        
        return precioFinal;
    }
        
       
        
    }