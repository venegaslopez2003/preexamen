/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Jesus Alberto Venegas Lopez
 */
public abstract class Nota {
    protected int numNota;
    protected String fecha;
    protected String concepto;
    protected Parecederos productop;
    protected int cantidad;
    protected int tipoPago;

    public Nota() {
        this.numNota = 0;
        this.fecha = "" ;
        this.concepto = "";
        this.productop = new Parecederos();
        this.cantidad = 0;
        this.tipoPago = 0;
       
    }

    public int getNumNota() {
        return numNota;
    }

    public void setNumNota(int numNota) {
        this.numNota = numNota;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public Parecederos getProductop() {
        return productop;
    }

    public void setProductop(Parecederos productop) {
        this.productop = productop;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(int tipoPago) {
        this.tipoPago = tipoPago;
    }
    
    public float calcularPago(){
        float total = 0;
 
        return total;
        
    }
    
    
    
    
    
    
}
